<?php

use App\Models\Token;
use Illuminate\Database\Schema\Blueprint;
use Phpmig\Migration\Migration;

class CreateTokenTable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $db = $container['db'];
        $db::schema()->create('token', function (Blueprint $table) {
            $table->id();
            $table->text('accessToken')->nullable();
            $table->text('refreshToken')->nullable();
            $table->string('mailchimpToken')->nullable();
            $table->string('baseDomain')->nullable();
            $table->integer('amocrm_client_id')->nullable();
            $table->timestamp('expires')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();
        $db = $container['db'];
        $db::schema()->drop('token');
    }
}
