<?php

use App\Models\Contact;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Phpmig\Migration\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $db = $container['db'];
        $db::schema()->create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->integer('amocrm_id');
            $table->integer('amocrm_client_id');
            $table->integer('imported')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();
        $db = $container['db'];
        $db::schema()->drop('contacts');
    }
}
