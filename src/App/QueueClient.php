<?php

declare(strict_types=1);

namespace App;

use Pheanstalk\Job;
use Pheanstalk\Pheanstalk;

class QueueClient
{
    private Pheanstalk $queue;

    public function __construct()
    {
        $this->queue = Pheanstalk::create('application-beanstalkd');
    }

    public function createExportTask(int $amocrmClientId): Job
    {
        $data = ['amocrm_id' => $amocrmClientId];
        return $this->queue->useTube('export_from_amocrm')->put(json_encode($data));
    }

    public function createImportTask(int $amocrmClientId): Job
    {
        $data = ['amocrm_id' => $amocrmClientId];
        return $this->queue->useTube('import_to_mailchimp')->put(json_encode($data));
    }

    public function getClient(): Pheanstalk
    {
        return $this->queue;
    }
}