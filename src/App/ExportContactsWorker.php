<?php

declare(strict_types=1);

namespace App;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Models\ContactModel;
use App\Models\Contact;
use App\Models\Database;
use App\Models\Email;
use App\Models\Token;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportContactsWorker extends Command
{
    private array $config;

    private AmoCRMApiClient $apiClient;

    private QueueClient $queueClient;

    public function __construct(array $config, AmoCRMApiClient $apiClient, QueueClient $queueClient)
    {
        parent::__construct();
        $this->config = $config;
        $this->apiClient = $apiClient;
        $this->queueClient = $queueClient;
        new Database($this->config);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $pheanstalk = $this->queueClient->getClient();
        try {
            $output->writeln("Export worker has started");
            $pheanstalk->watchOnly('export_from_amocrm');
            while (true) {
                $job = $pheanstalk
                    ->watch('export_from_amocrm')
                    ->ignore('default')
                    ->reserve();
                $jobData = json_decode($job->getData(), true);
                $start = microtime(true);
                try {
                    $token = Token::where('amocrm_client_id', '=', $jobData['amocrm_id'])->get()->first();
                    if (empty($token)) {
                        throw new \Exception("token not found");
                    }
                    $apiClient = $this->apiClient;
                    $token = new AccessToken([
                        'access_token' => $token['accessToken'],
                        'refresh_token' => $token['refreshToken'],
                        'expires' => $token['expires'],
                        'baseDomain' => $token['baseDomain'],
                    ]);
                    $apiClient->setAccountBaseDomain($token->getValues()['baseDomain']);
                    $apiClient->setAccessToken($token);
                    $this->deleteOldContacts($jobData['amocrm_id']);
                    $this->parseContacts($apiClient, $jobData['amocrm_id']);
                    $time = microtime(true) - $start;
                    $output->writeln("Сохранение в БД для пользователя {$jobData['amocrm_id']} успешно завершено за {$time} сек.");
                    $pheanstalk->delete($job);
                    $this->queueClient->createImportTask($jobData['amocrm_id']);
                } catch (\Exception $exception) {
                    $output->writeln("Ошибка: " . $exception->getMessage());
                }
            }
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
        return 0;
    }

    private function saveEmail(string $email, int $id): void
    {
        Email::create([
            'email' => $email,
            'contact_id' => $id
        ]);
    }

    private function parseContacts(AmoCRMApiClient $apiClient, int $amocrmClientId): array
    {
        $contacts = $apiClient->contacts()->get();
        $emailContact = [];
        foreach ($contacts as $contact) {
            $this->saveContact($contact, $amocrmClientId);
        }
        while (true) {
            try {
                $contacts = $apiClient->contacts()->nextPage($contacts);
                foreach ($contacts as $contact) {
                    $this->saveContact($contact, $amocrmClientId);
                }
            } catch (\AmoCRM\Exceptions\AmoCRMApiNoContentException $exception) {
                break;
            }
        }
        return $emailContact;
    }

    private function saveContact(ContactModel $contact, int $amocrmClientId): void
    {
        $email = $this->getEmail($contact);
        if (!empty($email)) {
            $newContact = Contact::create([
                'name' => $contact->getName(),
                'amocrm_id' => $contact->getId(),
                'amocrm_client_id' => $amocrmClientId
            ]);
            foreach ($email as $item) {
                $this->saveEmail($item, $newContact->id);
            }
        }
    }

    private function getEmail(ContactModel $contact): array
    {
        $result = false;
        $custom_fields = $contact->getCustomFieldsValues();
        if (is_null($custom_fields)) {
            return [];
        }
        $custom_fields = $custom_fields->toArray();
        foreach ($custom_fields as $item) {
            if ($item['field_name'] == 'Email') {
                $result = $item;
                break;
            }
        }
        if (!$result) {
            return [];
        }
        foreach ($item['values'] as $email) {
            $emails[] = $email['value'];
        }
        return $emails;
    }

    private function deleteOldContacts(int $clientId): void
    {
        Contact::where('amocrm_client_id', '=', $clientId)->delete();
    }
}