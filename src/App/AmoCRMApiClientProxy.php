<?php

declare(strict_types=1);

namespace App;

use AmoCRM\Client\AmoCRMApiClient;

class AmoCRMApiClientProxy extends AmoCRMApiClient
{
    private $config;

    function setAccessToken(\League\OAuth2\Client\Token\AccessToken $accessToken): AmoCRMApiClient
    {
        if ($accessToken->hasExpired()) {
            $this->updateAccessToken($accessToken);
        }
        return parent::setAccessToken($accessToken);
    }

    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    private function updateAccessToken(\League\OAuth2\Client\Token\AccessToken $accessToken): void
    {
        $accessToken = $this->getOAuthClient()->getAccessTokenByRefreshToken($accessToken);
        $time = time();
        $time = $this->getTime($time);
        $expires = $this->getTime($accessToken->getExpires());
        $dbh = new \PDO($this->config['db']['dsn'], $this->config['db']['user'], $this->config['db']['password']);
        $data = $dbh->prepare("UPDATE `token` SET `accessToken` = :accessToken,`refreshToken` = :refreshToken,`expires` = :expires,`updated_at` = :updated_at WHERE `amocrm_client_id` = :client_id");
        $data->bindParam(':accessToken', $accessToken->getToken());
        $data->bindParam(':refreshToken', $accessToken->getRefreshToken());
        $data->bindParam(":expires", $expires);
        $data->bindParam(":updated_at", $time);
        $data->bindParam(":client_id", $this->config['current_client_id']);
        $data->execute();
    }

    private function getTime(int $unix): int
    {
        $time = new \DateTime();
        $time->setTimestamp($unix);
        $time = $time->format('Y/m/d H:i:s');
        return $time;
    }
}