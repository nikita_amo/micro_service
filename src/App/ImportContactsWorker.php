<?php

declare(strict_types=1);

namespace App;

use App\Models\Contact;
use App\Models\Database;
use App\Models\Token;
use MailchimpTransactional\ApiClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportContactsWorker extends Command
{
    private array $config;

    private QueueClient $queueClient;

    public function __construct(array $config, QueueClient $queueClient)
    {
        parent::__construct();
        $this->queueClient = $queueClient;
        $this->config = $config;
        new Database($this->config);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pheanstalk = $this->queueClient->getClient();
        try {
            $output->writeln("Import worker has started");
            $pheanstalk->watchOnly('import_to_mailchimp');
            while (true) {
                $job = $pheanstalk
                    ->watch('import_to_mailchimp')
                    ->ignore('default')
                    ->reserve();
                $jobData = json_decode($job->getData(), true);
                try {
                    $token = Token::where('amocrm_client_id', '=', $jobData['amocrm_id'])->get()->first();
                    if (empty($token)) {
                        throw new \Exception("token not found");
                    }
                    $mail = new ApiClient();
                    $mail->setApiKey($token['mailchimpToken']);
                    if (!is_array($mail->ips->list())) {
                        throw new \Exception($jobData['amocrm_id'] . ' Invalid API Key ');
                    }
                    Contact::with('emails')
                        ->where('amocrm_client_id', '=', $jobData['amocrm_id'])
                        ->chunk('100', function ($contacts) use ($mail, $output) {
                            foreach ($contacts as $contact) {
                                foreach ($contact->emails as $email) {
                                    $output->write(1);
                                    $mail->allowlists->add(['email' => $email->email]);
                                }
                            }
                        });
                    $pheanstalk->delete($job);
                    $output->writeln('Импорт для пользователя ' . $jobData['amocrm_id'] . ' завершен');
                } catch (\Exception $exception) {
                    $output->writeln("Ошибка: " . $exception->getMessage());
                }
            }
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
        return 0;
    }
}