<?php

declare(strict_types=1);

namespace App\Handler;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Models\WebhookModel;
use App\Models\Token;
use App\QueueClient;
use DateTime;
use Laminas\Diactoros\Response\JsonResponse;
use MailchimpTransactional\ApiClient;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function time;

class WebHookHandler implements RequestHandlerInterface
{

    private array $config;

    private AmoCRMApiClient $apiClient;

    private QueueClient $queueClient;

    public function __construct(array $config, AmoCRMApiClient $apiClient, QueueClient $queueClient)
    {
        $this->config = $config;
        $this->apiClient = $apiClient;
        $this->queueClient = $queueClient;
    }


    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        try {
            if ($request->getParsedBody()) {
                return $this->saveMailChimpToken($request);
            }
            $get = $request->getQueryParams();
            if (empty($get['code'])) return new JsonResponse(['status' => false, 'message' => 'code doens\'t set']);
            $apiClient = $this->apiClient;
            $apiClient->setAccountBaseDomain($get['referer']);
            $oauthClient = $apiClient->getOAuthClient();
            $accessToken = $oauthClient->getAccessTokenByCode($get['code']);
            $accountId = $oauthClient->getAccountDomain($accessToken)->getId();
            $apiClient->setAccessToken($accessToken);
            $this->subscribeWebhook($apiClient);
            $data = $this->saveToken([
                'accessToken' => $accessToken->getToken(),
                'refreshToken' => $accessToken->getRefreshToken(),
                'expires' => $this->getTime($accessToken->getExpires()),
                'baseDomain' => $apiClient->getAccountBaseDomain(),
                'amocrm_client_id' => $accountId
            ]);
            $this->queueClient->createExportTask($accountId);
            return $data;
        } catch (\Exception $e) {
            return new JsonResponse(array('status' => true, 'message' => $e->getMessage()), 500);
        }
    }

    private function saveToken(array $array): ResponseInterface
    {
        try {
            $token = Token::firstOrCreate(
                ['amocrm_client_id' => $array['amocrm_client_id']],
            );
            $token->fill($array);
            $token->save();
            return new JsonResponse(array('status' => true, 'message' => 'token was set'), 200);
        } catch (\Exception $e) {
            return new JsonResponse(array('status' => true, 'message' => $e->getMessage()), 500);
        }
    }

    private function getTime(int $unix): DateTime
    {
        $time = new DateTime();
        return $time->setTimestamp($unix);
    }

    public function saveMailChimpToken(ServerRequestInterface $request): ResponseInterface
    {
        $xAuthToken = $request->getParsedBody()['x_auth_token'];
        $apiClient = $this->apiClient;
        try {
            $apiClient->getOAuthClient()->parseDisposableToken($xAuthToken);
        } catch (\Exception $e) {
            return new JsonResponse(['status' => false, 'message' => $e->getMessage()]);
        }
        $post = $request->getParsedBody();
        $mail = new ApiClient();
        $mail->setApiKey($post['token']);
        if (!is_array($mail->ips->list())) {
            return new JsonResponse(array('status' => false, 'message' => 'invalid api key'), 400);
        }
        return $this->saveToken([
            'amocrm_client_id' => $post['user_id'],
            'mailchimpToken' => $post['token'],
        ]);
    }

    private function subscribeWebhook(AmoCRMApiClient $apiClient): void
    {
        $webhook = new WebhookModel();
        $webhook->setDestination($this->config['redirect_url'] . '/contact')
            ->setSettings([
                'add_contact',
                'update_contact',
                'delete_contact'
            ]);
        $webhook = $apiClient->webhooks()->subscribe($webhook);
    }
}
