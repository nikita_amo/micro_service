<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AddHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $this->add($request->getParsedBody());
        return new JsonResponse($data, $data['code']);
    }

    private function add(array $numbers): array
    {
        $result = 0;
        foreach ($numbers as $item) {
            if (!is_numeric($item)) {
                return array(
                    'status' => false,
                    'error' => 'field must have only number',
                    'code' => 400
                );
            }
            $result += $item;
        }
        return array('status' => true, 'result' => $result, 'code' => 200);
    }
}
