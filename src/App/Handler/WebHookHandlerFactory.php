<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;

class WebHookHandlerFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        return new WebHookHandler($container->get('config'), $container->get('AmoCRMApiClient'), $container->get('QueueClient'));
    }
}