<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ClientWebHookFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        return new ClientWebhookHandler($container->get('config'), $container->get('AmoCRMApiClient'));
    }
}