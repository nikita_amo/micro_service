<?php

declare(strict_types=1);

namespace App\Handler;

use AmoCRM\Client\AmoCRMApiClient;
use App\AmoCRMApiClientProxy;
use App\Models\Contact;
use App\Models\Email;
use App\Models\Token;
use Laminas\Diactoros\Response\JsonResponse;
use League\OAuth2\Client\Token\AccessToken;
use MailchimpTransactional\ApiClient;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ClientWebhookHandler implements RequestHandlerInterface
{

    private array $config;

    private ApiClient $mail;

    private AmoCRMApiClient $apiClient;

    public function __construct(array $config, AmoCRMApiClient $apiClient)
    {
        $this->config = $config;
        $this->apiClient = $apiClient;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $post = $request->getParsedBody();
        $accountId = $post['account']['id'];
        $row = Token::where('amocrm_client_id', '=', $accountId)->get()->first();
        if ('https://' . $row['baseDomain'] != $post['account']['_links']['self']) {
            return new JsonResponse(array('status' => false, 'message' => ''), 403);
        }
        $apiClient = $this->apiClient;
        $token = new AccessToken([
            'access_token' => $row['accessToken'],
            'refresh_token' => $row['refreshToken'],
            'expires' => $row['expires'],
            'baseDomain' => $row['baseDomain'],
        ]);
        $apiClient->setAccessToken($token);
        $apiClient->setAccountBaseDomain($token->getValues()['baseDomain']);
        $this->mail = new ApiClient();
        $this->mail->setApiKey($row['mailchimpToken']);
        if (!is_array($this->mail->ips->list())) {
            throw new \Exception($accountId . ' Invalid API Key ');
        }
        $action = array_keys($post['contacts'])[0];
        if (!in_array($action, ['delete', 'add', 'update'])) {
            return new JsonResponse(array('status' => false, 'message' => 'action not available'), 400);
        }
        try {
            return $this->$action($post);
        } catch (\Exception $e) {
            return new JsonResponse(array('status' => false, 'message' => $e->getMessage()), 500);
        }
    }

    private function update(array $post): ResponseInterface
    {
        $contacts = $post['contacts']['update'];
        foreach ($contacts as $contact) {
            $oldContact = Contact::where('amocrm_id', '=', $contact['id'])->get()->first();
            if ($contact['name'] != $oldContact['name']) {
                $oldContact->name = $contact['name'];
                $oldContact->save();
            }
            $newEmails = $this->getEmail($contact['custom_fields']);
            $oldEmails = $oldContact->emails;
            $oldEmailsArray = $oldEmails->pluck('email')->toArray();
            if (!empty(array_diff($newEmails, $oldEmailsArray))) {
                foreach ($oldEmails as $email) {
                    $this->mail->allowlists->delete(['email' => $email->email]);
                    $email->delete();
                }
                foreach ($newEmails as $email) {
                    Email::create([
                        'email' => $email,
                        'contact_id' => $oldContact->id
                    ]);
                    $this->mail->allowlists->add(['email' => $email]);
                }
            }
        }
        return new JsonResponse(array('status' => true), 200);
    }

    private function delete(array $post): ResponseInterface
    {
        $contacts = $post['contacts']['delete'];
        foreach ($contacts as $contact) {
            $oldContact = Contact::where('amocrm_id', '=', $contact['id'])->get()->first();
            foreach ($oldContact->emails as $email) {
                $this->mail->allowlists->delete(['email' => $email->email]);
            }
            $oldContact->delete();
        }
        return new JsonResponse(array('status' => true), 200);
    }

    private function add(array $post): ResponseInterface
    {
        $contacts = $post['contacts']['add'];
        foreach ($contacts as $contact) {
            $newContact = new Contact();
            $newContact->name = $contact['name'];
            $emails = $this->getEmail($contact['custom_fields']);
            $newContact->amocrm_id = $contact['id'];
            $newContact->amocrm_client_id = $post['account']['id'];
            $newContact->save();
            foreach ($emails as $email) {
                Email::create([
                    'email' => $email,
                    'contact_id' => $newContact->id
                ]);
                $this->mail->allowlists->add(['email' => $email]);
            }
        }
        return new JsonResponse(array('status' => true), 200);
    }

    private function getEmail(array $custom_fields): array
    {
        $result = false;
        foreach ($custom_fields as $item) {
            if ($item['name'] == 'Email') {
                $result = $item;
                break;
            }
        }
        if (!$result) {
            return [];
        }
        foreach ($item['values'] as $email) {
            $emails[] = $email['value'];
        }
        return $emails;
    }
}