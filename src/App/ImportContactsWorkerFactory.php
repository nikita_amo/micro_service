<?php

declare(strict_types=1);

namespace App;

use Psr\Container\ContainerInterface;

class ImportContactsWorkerFactory
{
    public function __invoke(ContainerInterface $container): ImportContactsWorker
    {
        return new ImportContactsWorker($container->get('config'), $container->get('QueueClient'));
    }
}