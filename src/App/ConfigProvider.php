<?php

declare(strict_types=1);

namespace App;


use App\Handler\ClientWebHookFactory;
use App\Handler\ClientWebhookHandler;
use App\Handler\HomePageHandler;
use App\Handler\HomePageHandlerFactory;
use App\Handler\PingHandler;
use App\Handler\WebHookHandler;
use App\Handler\WebHookHandlerFactory;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.laminas.dev/laminas-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates' => $this->getTemplates(),
            'laminas-cli' => ['commands' => [
                'worker:export' => ExportContactsWorker::class,
                'worker:import' => ImportContactsWorker::class,
            ],
            ],
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'invokables' => [
                PingHandler::class => PingHandler::class,
            ],
            'factories' => [
                HomePageHandler::class => HomePageHandlerFactory::class,
                WebHookHandler::class => WebHookHandlerFactory::class,
                ExportContactsWorker::class => ExportContactsWorkerFactory::class,
                ImportContactsWorker::class => ImportContactsWorkerFactory::class,
                ClientWebhookHandler::class => ClientWebHookFactory::class,
                'AmoCRMApiClient' => AmoCRMClientFactory::class,
                'QueueClient' => QueueClientFactory::class
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'app' => [__DIR__ . '/templates/app'],
                'error' => [__DIR__ . '/templates/error'],
                'layout' => [__DIR__ . '/templates/layout'],
            ],
        ];
    }
}
