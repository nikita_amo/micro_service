<?php

namespace App;


class QueueClientFactory
{
    public function __invoke()
    {
        return new QueueClient();
    }
}