<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = "email";
    protected $guarded = ['id'];

    public function contact()
    {
        return $this->belongsToMany(Contact::class);
    }
}