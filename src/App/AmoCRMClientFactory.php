<?php

namespace App;

use Psr\Container\ContainerInterface;

class AmoCRMClientFactory
{
    public function __invoke(ContainerInterface $container): AmoCRMApiClientProxy
    {
        $config = $container->get('config');
        $clientId = $config['client_id'];
        $clientSecret = $config['client_secret'];
        $redirectUri = $config['redirect_url'];
        $apiClient = new AmoCRMApiClientProxy($clientId, $clientSecret, $redirectUri);
        $apiClient->setConfig($config);
        return $apiClient;
    }
}