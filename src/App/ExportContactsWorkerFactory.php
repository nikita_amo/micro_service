<?php

declare(strict_types=1);

namespace App;

use Psr\Container\ContainerInterface;

class ExportContactsWorkerFactory
{
    public function __invoke(ContainerInterface $container): ExportContactsWorker
    {
        $apiClient = $container->get('AmoCRMApiClient');
        $queueClient = $container->get('QueueClient');
        return new ExportContactsWorker($container->get('config'), $apiClient, $queueClient);
    }
}