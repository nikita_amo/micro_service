<?php

# phpmig.php

use Phpmig\Adapter;
use Pimple\Container;
use App\Models\Database;

$container = new Container();

$container['db'] = function () {
    $db = new Database(require 'config/autoload/local.php');
    return $db->getCapsule();
};

$container['phpmig.adapter'] = function ($c) {
    return new Adapter\Illuminate\Database($c['db'], 'migrations');
};
$container['phpmig.migrations_path'] = __DIR__ . DIRECTORY_SEPARATOR . 'migrations';

return $container;
